import { Injectable } from '@nestjs/common';
import { Connection, DeleteQueryBuilder } from 'typeorm';
import { Image } from './db/image.entity';
import { Quiz } from './db/quiz.entity';
import { SubmitDTO } from 'submit.dto';
import { QuizAnswered } from './db/quiz-answered.entity';
import { QuestionAnswered } from './db/question-answered.entity';
import { AnswerAnswered } from './db/answer-answered.entity';
import { User } from './db/user.entity';

@Injectable()
export class AppService {
  constructor(private connection: Connection) { }


  getImagePath(id: number): Promise<Image> {
    const imageRepo = this.connection.getRepository(Image);
    return imageRepo.findOneOrFail({ id: id });
  }

  getAllQuizzes(): Promise<Quiz[]> {
    return new Promise((resolve, reject) => {
      const quizRepo = this.connection.getRepository(Quiz);
      return quizRepo.find({ select: ["id", "title", "imageId"], order: { created: "DESC" } }).then(quizzes => {
        resolve(quizzes);
      }).catch(err => reject("Exception DB Query"));
    });
  }

  getQuiz(id: number): Promise<Quiz> {
    return new Promise((resolve, reject) => {

      this.connection.getRepository(Quiz).createQueryBuilder("quiz")
        .innerJoinAndSelect("quiz.questions", "question")
        .innerJoinAndSelect("question.answers", "answer")
        .where({ id: id })
        .getOne()
        .then(quiz => {
          resolve(quiz);
        }).catch(err => reject("Exception DB Query"));
    });
  }

  getQuizWithAnswers(id: number): Promise<Quiz> {
    return new Promise((resolve, reject) => {
      this.connection.getRepository(Quiz).createQueryBuilder("quiz")
        .innerJoinAndSelect("quiz.questions", "question")
        .innerJoinAndSelect("question.answers", "answer")
        .addSelect("answer.right")
        .where({ id: id })
        .getOne()
        .then(quiz => {
          resolve(quiz);
        }).catch(err => reject("Exception DB Query"));
    });
  }

  submitQuiz(submit: SubmitDTO, userID: number): Promise<any> {
    return new Promise((resolve, reject) => {
      let points = 0;
      let result: { id: number, right: boolean, answers: { id: number }[] }[] = [];
      this.getQuizWithAnswers(submit.id).then(quiz => {
        if (quiz.questions.length != submit.questions.length) {
          reject("Not all questions are answered yet!");
        } else {
          // für jede frage, die vom user beantwortet wurde
          quiz.questions.forEach(q => {
            // prüfe alle antowrten
            let userQuestion = submit.questions.find(sq => sq.id === q.id);
            if (userQuestion) {
              console.log("validate question", q.id, q.answers, userQuestion.answers);
              let rightAnswered = this.checkRightAnswers(userQuestion.answers, q.answers.filter(r => r.right));
              if (rightAnswered) {
                points += q.points;
              }
              result.push({ id: q.id, right: rightAnswered, answers: userQuestion.answers });
            } else {
              reject("Failed to find right question")
              return;
            }
          });
          this.saveAnswerInDB(submit, result, points, userID);
          resolve({ points: points, questions: result, id: quiz.id, title: quiz.title });
        }
      }).catch(err => {
        reject("No Quiz found for this id");
      });
    });
  }

  private checkRightAnswers(userAnswers: { id: number }[], rightAnswers: { id: number }[]): boolean {
    if (userAnswers.length != rightAnswers.length) {
      return false;
    }
    for (let i = 0; i < userAnswers.length; i++) {
      const ua = userAnswers[i];
      const index = rightAnswers.findIndex(ra => ra.id === ua.id);
      if (index < 0) {
        return false;
      }
    }
    return true;
  }

  private saveAnswerInDB(submit: SubmitDTO, questions: { id: number, right: boolean, answers: { id: number }[] }[], points: number, userID: number) {
    const quizAnswered = new QuizAnswered();
    quizAnswered.quizId = submit.id
    quizAnswered.submitted = new Date();
    quizAnswered.questionAnswered = [];
    questions.forEach(q => {
      const qa = new QuestionAnswered();
      qa.questionId = q.id;
      qa.answeredAt = new Date()
      qa.right = q.right;
      qa.answers = [];
      q.answers.forEach(a => {
        const ua = new AnswerAnswered();
        ua.answerId = a.id;
        qa.answers.push(ua);
      });
      quizAnswered.questionAnswered.push(qa);
    });
    const qaRepo = this.connection.getRepository(QuizAnswered);
    qaRepo.save(quizAnswered);
    const userRepo = this.connection.getRepository(User);
    userRepo.findOne({ id: userID }).then(user => {
      user.points += points;
      userRepo.save(user);
    }).catch(err => console.error(err));
  }
}
