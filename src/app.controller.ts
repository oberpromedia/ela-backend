import { Get, Controller, Post, Res, Param, HttpStatus, UseGuards, ValidationPipe, Body, Req } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthGuard } from '@nestjs/passport';
import { SubmitDTO } from './submit.dto';

@Controller('app')
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get('/get-all-quizzes')
  @UseGuards(AuthGuard('bearer'))
  getAllQuizzes(@Res() res) {
    this.appService.getAllQuizzes().then(quizzes => {
      res.status(HttpStatus.OK).json(quizzes);
    }).catch(err => {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({});
    });
  }

  @Get('get-quiz/:id')
  @UseGuards(AuthGuard('bearer'))
  getQuiz(@Param('id') id, @Res() res) {
    this.appService.getQuiz(id).then(quiz => {
      res.status(HttpStatus.OK).json(quiz);
    }).catch(err => {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({});
    });
  }

  @Get('images/:imgId')
  getImage(@Param('imgId') imgId, @Res() res) {
    this.getImgPath(imgId).then(path => {
      res.sendFile(path, { root: 'public' });
    }).catch(err => { res.status(HttpStatus.NOT_FOUND).json({}) });
  }

  private getImgPath(id: number): Promise<string> {
    return new Promise((resolve, reject) => {
      this.appService.getImagePath(id).then(image => {
        resolve("/" + image.name);
      }).catch(err => reject("Image not found"));
    });
  }

  @Post('submit')
  @UseGuards(AuthGuard('bearer'))
  submitQuiz(@Body(new ValidationPipe()) data: SubmitDTO, @Req() req, @Res() res) {
    this.appService.submitQuiz(data, req.user.id).then(_res => {
      res.status(HttpStatus.OK).json({
        status: true,
        data: _res
      })
    }).catch(err => {
      res.status(HttpStatus.OK).json({
        status: false,
        reason: "Failed to validate quiz",
      })
      console.error((err));
    });
  }

}
