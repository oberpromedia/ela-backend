import { IsNotEmpty, MinLength } from "class-validator";

export class UserLoginDto {
    @IsNotEmpty()
    username: string;
    @IsNotEmpty()
    @MinLength(6)
    password: string;
}