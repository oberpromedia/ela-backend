import { Controller, ValidationPipe, Body, Post, Res, HttpStatus, UseGuards, Req, UseFilters, Get } from '@nestjs/common';
import { UserService } from '../services/user.service';
import { UserLoginDto } from './user-login.dto';
import { AuthGuard } from '@nestjs/passport';
import { HttpExceptionFilter } from '../strategy/http-exception-filter';

@Controller('auth')
export class AuthController {
    constructor(private readonly _user: UserService) {
    }

    @Post('/login')
    @UseFilters(new HttpExceptionFilter())
    loginUser(@Body(new ValidationPipe()) anfrage: UserLoginDto, @Res() response) {
        this._user.loginUser(anfrage.username, anfrage.password).then(user => {
            response.status(HttpStatus.OK).json(user);
        }).catch(err => {
            response.status(500).json({ error: err });
        });
    }

    @Post('/logout')
    @UseGuards(AuthGuard('bearer'))
    logoutUser(@Req() request, @Res() response) {
        this._user.logoutUser(request.user.user_id).then(user => {
            response.status(HttpStatus.OK).json({ logout: user });
        }).catch(err => {
            response.status(500).json({ error: err });
        });
    }

    @Get('/get')
    @UseGuards(AuthGuard('bearer'))
    getCurrentUser(@Req() request, @Res() response) {
        if (this._user.isPerformanceTest()) {
            this._user.performanceTest().then(user => {
                response.status(HttpStatus.OK).json(user);
            }).catch(err => {
                response.status(500).json({ error: err });
            });
        } else {
            response.status(HttpStatus.OK).json(request.user);
        }
    }
}
