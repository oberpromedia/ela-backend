import { IsNotEmpty, IsPositive } from "class-validator";

export class SubmitAnswerDTO {
    @IsNotEmpty()
    @IsPositive()
    id: number;
}