import { IsNotEmpty, IsPositive, IsArray } from "class-validator";
import { SubmitQuestionDTO } from "./submit-question.dto";

export class SubmitDTO {
    @IsNotEmpty()
    @IsPositive()
    id: number;
    @IsNotEmpty()
    @IsArray()
    questions: SubmitQuestionDTO[];
}