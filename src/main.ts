import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as helmet from 'helmet';
import { join } from 'path';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(helmet());
  app.enableCors();
  console.log('using configuration', process.env);
  app.useStaticAssets(join(__dirname, '/../public'));
  await app.listen(3000);
}
bootstrap();
