import { IsNotEmpty, IsPositive, IsArray } from "class-validator";
import { SubmitAnswerDTO } from "./submit-answer.dto";

export class SubmitQuestionDTO {
    @IsNotEmpty()
    @IsPositive()
    id: number;
    @IsNotEmpty()
    @IsArray()
    answers: SubmitAnswerDTO[]
}