import { Injectable } from '@nestjs/common';
import { TypeOrmOptionsFactory, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { User } from '../db/user.entity';
import { Quiz } from '../db/quiz.entity';
import { Image } from '../db/image.entity';
import { Question } from '../db/question.entity';
import { Answer } from '../db/answer.entity';
import { QuizAnswered } from '../db/quiz-answered.entity';
import { QuestionAnswered } from '../db/question-answered.entity';
import { AnswerAnswered } from '../db/answer-answered.entity';
@Injectable()
export class OrmConfigService implements TypeOrmOptionsFactory {

    createTypeOrmOptions(): Promise<TypeOrmModuleOptions> {
        return new Promise((resolve, reject) => {
            resolve({
                type: 'mysql',
                host: process.env.DB_HOST,
                port: parseInt(process.env.DB_PORT, 10),
                username: process.env.DB_USERNAME,
                password: process.env.DB_PASSWORD,
                database: process.env.DB_DATABASE,
                entities: [User, Image, Quiz, Question, Answer, QuizAnswered, QuestionAnswered, AnswerAnswered],
                synchronize: true
            });
        });
    }
}
