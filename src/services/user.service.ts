import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { User } from '../db/user.entity';
const crypto = require('crypto');

@Injectable()
export class UserService {

    constructor(private connection: Connection) { }

    findByToken(token: string): Promise<User> {
        return new Promise((resolve, reject) => {
            const userRepo = this.connection.getRepository(User);
            userRepo.findOne({ token: token }).then(user => {
                delete user.password;
                resolve(user);
            }).catch(err => {
                resolve(null);
            });
        });
    }

    isPerformanceTest(): boolean {
        return !!process.env.PERFORMANCE_TEST === true;
    }

    performanceTest(): Promise<User> {
        return new Promise((resolve, reject) => {
            if (this.isPerformanceTest()) {
                const userRepo = this.connection.getRepository(User);
                userRepo.findOne().then(user => {
                    delete user.password;
                    resolve(user);
                }).catch(err => {
                    resolve(null);
                });
            } else {
                resolve(null);
            }
        });
    }

    loginUser(username: string, password: string): Promise<User> {
        return new Promise((resolve, reject) => {
            const userRepo = this.connection.getRepository(User);
            userRepo.findOne({ username: username.toLowerCase() }).then(user => {
                if (user && user.password === this.hashPassword(password)) {
                    user.token = this.generateToken();
                    userRepo.update(user.id, { token: user.token });
                    delete user.password;
                    resolve(user);
                } else {
                    reject("Username or Password is wrong");
                }
            }).catch(err => {
                console.error(err);
                reject("Login failed!");
            });
        });
    }

    logoutUser(user_id: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            const userRepo = this.connection.getRepository(User);
            userRepo.update(user_id, { token: null }).then(res => {
                resolve(true);
            }).catch(err => { reject("Logout failed!") });
        });
    }

    private generateToken() {
        let randomToken = "";
        for (let i = 0; i < 128; i++) {
            randomToken += String.fromCharCode(this.getRandomInt(88) + 38);
        }
        return crypto.createHmac('sha512', '3uvcnj290vj20(3kjU/84Nfj4558NFJ<555jh()857(§dkf')
            .update(randomToken)
            .digest('hex');
    }
    private hashPassword(pw: string) {
        return crypto.createHmac('sha512', 'top-secret')
            .update(pw)
            .digest('hex');
    }
    private getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }
}
