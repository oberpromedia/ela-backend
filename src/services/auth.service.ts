import { Injectable } from '@nestjs/common';
import { UserService } from './user.service';

@Injectable()
export class AuthService {
    constructor(private readonly userService: UserService) { }

    async validateUser(token: string): Promise<any> {
        if (this.userService.isPerformanceTest()) {
            return await this.userService.performanceTest();
        }
        return await this.userService.findByToken(token);
    }
}
