import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne } from "typeorm";
import { QuestionType } from "./question-type.enum";
import { Answer } from "./answer.entity";
import { Quiz } from "./quiz.entity";

@Entity('question')
export class Question {
    @PrimaryGeneratedColumn()
    id: number;
    @Column('text', { nullable: false })
    text: string;
    @Column('int', { nullable: false })
    type: QuestionType;
    @OneToMany(type => Answer, answer => answer.question)
    answers: Answer[];
    @ManyToOne(type => Quiz, quiz => quiz.questions)
    quiz: Quiz;
    @Column('int', { nullable: false })
    points: number;
}