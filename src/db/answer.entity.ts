import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { QuestionType } from "./question-type.enum";
import { Question } from "./question.entity";

@Entity('answer')
export class Answer {
    @PrimaryGeneratedColumn()
    id: number;
    @Column('text', { nullable: false })
    text: string;
    @Column('tinyint', { nullable: false, select: false })
    right: boolean;
    @ManyToOne(type => Question, question => question.answers)
    question: Question;
}