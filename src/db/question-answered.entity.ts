import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne, OneToMany } from "typeorm";
import { Quiz } from "./quiz.entity";
import { Question } from "./question.entity";
import { QuizAnswered } from "./quiz-answered.entity";
import { AnswerAnswered } from "./answer-answered.entity";

@Entity('question_answered')
export class QuestionAnswered {
    @PrimaryGeneratedColumn()
    id: number;
    @ManyToOne(type => Question, question => question.id)
    @JoinColumn({ name: "questionId" })
    question: Question;
    @Column({ nullable: true })
    questionId: number;
    @ManyToOne(type => QuizAnswered, quiz => quiz.questionAnswered)
    quizAnswered: QuizAnswered;
    @OneToMany(type => AnswerAnswered, answer => answer.questionAnswered)
    answers: AnswerAnswered[];
    @Column('datetime', { nullable: false })
    answeredAt: Date;
    @Column('tinyint', { nullable: true })
    right: boolean;
}