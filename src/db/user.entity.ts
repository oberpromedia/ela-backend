import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity('user')
export class User {
    @PrimaryGeneratedColumn()
    id: number;
    @Column('text', { nullable: true })
    token: string;
    @Column('varchar', { length: 85, nullable: false })
    vorname: string;
    @Column('varchar', { length: 85, nullable: false })
    nachname: string;
    @Column('varchar', { length: 45, nullable: false })
    username: string;
    @Column('text', { nullable: false })
    password: string;
    @Column('int', { nullable: false })
    points: number;
}