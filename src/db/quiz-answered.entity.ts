import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne, OneToMany } from "typeorm";
import { Quiz } from "./quiz.entity";
import { QuestionAnswered } from "./question-answered.entity";

@Entity('quiz_answered')
export class QuizAnswered {
    @PrimaryGeneratedColumn()
    id: number;
    @ManyToOne(type => Quiz, quiz => quiz.id)
    @JoinColumn({ name: "quizId" })
    quiz: Quiz;
    @Column({ nullable: true })
    quizId: number;
    @OneToMany(type => QuestionAnswered, question => question.quizAnswered)
    questionAnswered: QuestionAnswered[];
    @Column('datetime', { nullable: false })
    submitted: Date;
}