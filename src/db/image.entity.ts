import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity('image')
export class Image {
    @PrimaryGeneratedColumn()
    id: number;
    @Column('varchar', { length: 500, nullable: false })
    name: string;
    @Column('datetime', { nullable: false })
    created: Date;
}