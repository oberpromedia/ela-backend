import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from "typeorm";
import { QuestionType } from "./question-type.enum";
import { Question } from "./question.entity";
import { QuestionAnswered } from "./question-answered.entity";
import { Answer } from "./answer.entity";

@Entity('answer_answered')
export class AnswerAnswered {
    @PrimaryGeneratedColumn()
    id: number;
    @ManyToOne(type => Answer, answer => answer.id)
    @JoinColumn({ name: "answerId" })
    answer: Answer;
    @Column({ nullable: true })
    answerId: number;
    @ManyToOne(type => QuestionAnswered, question => question.answers)
    questionAnswered: QuestionAnswered;
}