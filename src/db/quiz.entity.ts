import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, ManyToOne, OneToMany } from "typeorm";
import { Image } from "./image.entity";
import { Question } from "./question.entity";

@Entity('quiz')
export class Quiz {
    @PrimaryGeneratedColumn()
    id: number;
    @Column('text', { nullable: false })
    title: string;
    @ManyToOne(type => Image, image => image.id)
    @JoinColumn({ name: "imageId" })
    image: Image;
    @Column({ nullable: true })
    imageId: number;
    @Column('datetime', { nullable: false, select: false })
    created: Date;
    @OneToMany(type => Question, question => question.quiz)
    questions: Question[];
}