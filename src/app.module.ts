import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrmConfigService } from './services/orm-config.service';
import { AuthController } from './auth/auth.controller';
import { HttpStrategy } from './strategy/auth-strategy';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';
import { HttpExceptionFilter } from './strategy/http-exception-filter';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      useClass: OrmConfigService
    }),
  ],
  controllers: [AppController, AuthController],
  providers: [AuthService, UserService, AppService, OrmConfigService, HttpStrategy, HttpExceptionFilter],
})
export class AppModule { }
