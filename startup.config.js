module.exports = {
  apps: [{
    name: 'e-learning-backend',
    script: 'dist/main.js',
    args: '',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development',
      DB_HSOT: 'localhost',
      DB_PORT: 3306,
      DB_USERNAME: 'e-learner',
      DB_PASSWORD: 'Acc123Bla?',
      DB_DATABASE: 'e-learning',
      PERFORMANCE_TEST: false
    },
    env_production: {
      NODE_ENV: 'production',
      DB_HSOT: 'localhost',
      DB_PORT: 3306,
      DB_USERNAME: 'e-learner',
      DB_PASSWORD: 'Acc123Bla?',
      DB_DATABASE: 'e-learning',
      PERFORMANCE_TEST: false
    },
    env_performance: {
      NODE_ENV: 'production',
      DB_HSOT: 'localhost',
      DB_PORT: 3306,
      DB_USERNAME: 'e-learner',
      DB_PASSWORD: 'Acc123Bla?',
      DB_DATABASE: 'e-learning',
      PERFORMANCE_TEST: true
    }
  }]
};